﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class HuespedRequest
    {
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string correo { get; set; }
        public string pais { get; set; }
        public int telefono { get; set; }
        public string ciudad { get; set; }
        public string comentario { get; set; }
        public int id_reservas { get; set; }

        //`nombre`, `apellido`, `correo`, `pais`, `telefono`, `ciudad`, `comentario`, `id_reservas` 
    }
}