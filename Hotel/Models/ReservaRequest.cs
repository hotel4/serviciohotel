﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class ReservaRequest
    {
        public int id_habitacion { get; set; }
        public DateTime fecha_entrada { get; set; }
        public DateTime fecha_salida { get; set; }
        public int ocupacion_adulto { get; set; }
        public int ocupacion_nino { get; set; }
        public double precio_final { get; set; }

        public HuespedRequest usuario { get; set; }
        //id_habitacion`, `fecha_entrada`, `fecha_salida`, `ocupacion_adulto`, `ocupacion_nino`, `precio_final`
    }
}