﻿using Dapper;
using Hotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.AccesoDatos
{
    public class Huesped: Conexion
    {
        public dynamic insertarHuesped(HuespedRequest value)
        {
            try
            {
                var _parametros = new DynamicParameters();
                _parametros.Add("@nombre", value.nombre);
                _parametros.Add("@apellido", value.apellido);
                _parametros.Add("@correo", value.correo);
                _parametros.Add("@pais", value.pais);
                _parametros.Add("@telefono", value.telefono);
                _parametros.Add("@ciudad", value.ciudad);
                _parametros.Add("@comentario", value.comentario);
                _parametros.Add("@id_reservas", value.id_reservas);
                this.conectar();
                return this.connection.Execute(@"INSERT INTO  `hotel`.`huespedes` (`nombre`, `apellido`, `correo`, `pais`, `telefono`, `ciudad`, `comentario`, `id_reservas`) 
                                                VALUES (@nombre, @apellido, @correo, @pais, @telefono, @ciudad, @comentario, @id_reservas);",_parametros );
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                this.cerrarConexion();
            }
        }
    }
}