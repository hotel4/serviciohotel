﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.AccesoDatos
{
    public class Habitacion: Conexion
    {
        public dynamic consultarHabitaciones() {
            try
            {
                this.conectar();
                return this.connection.Query("select * from habitaciones");
            }
            catch (Exception)
            {

                throw;
            }
            finally {
                this.cerrarConexion();
            }
        }
    }
}