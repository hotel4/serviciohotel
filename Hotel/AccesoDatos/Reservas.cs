﻿using Dapper;
using Hotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.AccesoDatos
{
    public class Reservas: Conexion
    {
        public int insertarReserva(ReservaRequest value)
        {
            try
            {
                var _parametros = new DynamicParameters();
                _parametros.Add("@id_habitacion", value.id_habitacion);
                _parametros.Add("@fecha_entrada", value.fecha_entrada);
                _parametros.Add("@fecha_salida", value.fecha_salida);
                _parametros.Add("@ocupacion_adulto", value.ocupacion_adulto);
                _parametros.Add("@ocupacion_nino", value.ocupacion_nino);
                _parametros.Add("@precio_final", value.precio_final);
                this.conectar();
                return this.connection.ExecuteScalar<int>(@"INSERT INTO `hotel`.`reservas` (`id_habitacion`, `fecha_entrada`, `fecha_salida`, `ocupacion_adulto`, `ocupacion_nino`, `precio_final`)
                                                VALUES (@id_habitacion, @fecha_entrada, @fecha_salida, @ocupacion_adulto, @ocupacion_nino, @precio_final);
                                                SELECT LAST_INSERT_ID();", _parametros);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                this.cerrarConexion();
            }
        }
    }
}