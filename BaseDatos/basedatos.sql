-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.18 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para hotel
DROP DATABASE IF EXISTS `hotel`;
CREATE DATABASE IF NOT EXISTS `hotel` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `hotel`;

-- Volcando estructura para tabla hotel.habitaciones
DROP TABLE IF EXISTS `habitaciones`;
CREATE TABLE IF NOT EXISTS `habitaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ocupacion_adultos_max` int(11) DEFAULT NULL,
  `ocupacion_ninos_max` int(11) DEFAULT NULL,
  `descripcion` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `decripcion_corta` varchar(100) DEFAULT NULL,
  `precio` varchar(50) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hotel.habitaciones: ~3 rows (aproximadamente)
DELETE FROM `habitaciones`;
/*!40000 ALTER TABLE `habitaciones` DISABLE KEYS */;
INSERT INTO `habitaciones` (`id`, `ocupacion_adultos_max`, `ocupacion_ninos_max`, `descripcion`, `decripcion_corta`, `precio`, `estado`) VALUES
	(1, 4, 2, 'Las habitaciones de dos camas dobles vista al mar del Hotel Paradise tienen una fabulosa vista a la costa caribeña, lo que las convierte en el alojamiento perfecto para familias de viaje en Colombia. Disfrute de una habitación equipada con 2 camas dobles de 1,40 metros, escritorio y silla, internet Wi-Fi, pantalla plana de 32’’, closet con caja de seguridad, minibar y radio despertador.', 'HABITACIÓN ESTÁNDAR DOS CAMAS DOBLES VISTA AL MAR', '249999', 1),
	(2, 2, 2, 'La habitación estándar de dos cama sencillas vista al mar del Hotel Paradise, con 25 m² de superficie, es el alojamiento idóneo para personas que buscan una cama individual. Estas habitaciones en Cartagena están equipadas con balcón, internet Wi-Fi, televisión con pantalla plana de 32 pulgadas, aire acondicionado y minibar.', 'HABITACIÓN ESTÁNDAR DOS CAMAS SENCILLAS VISTA AL MAR', '199900', 1),
	(3, 2, 2, 'Si está buscando un espacio íntimo y espacioso para su viaje en pareja a Cartagena, las habitaciones estándar una cama King con vista al mar del Hotel Paradise son su mejor opción de alojamiento en esta hermosa zona de Cartagena. Haga su reserva ahora y saque el máximo partido a su hermoso balcón con vista al mar, aire acondicionado, closet con caja de seguridad, televisión de 32 pulgadas LCD, internet Wi-Fi…', 'HABITACIÓN ESTÁNDAR UNA CAMA KING VISTA AL MAR', '289000', 1),
	(4, 2, 2, 'Las junior suite con cama King vista al mar del Hotel Paradise son habitaciones espaciosas de 36 m² con balcón y sala con sofá cama. Además, estas habitaciones ofrecen todas las amenidades de nuestro hotel vacacional: internet Wi-Fi, televisión pantalla plana de 32 pulgadas, aire acondicionado, closet con caja de seguridad, minibar, radio despertador, teléfono directo y baño totalmente equipado', 'JUNIOR SUITE CON CAMA KING VISTA AL MAR', '320000', 1);
/*!40000 ALTER TABLE `habitaciones` ENABLE KEYS */;

-- Volcando estructura para tabla hotel.huespedes
DROP TABLE IF EXISTS `huespedes`;
CREATE TABLE IF NOT EXISTS `huespedes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `comentario` varchar(200) DEFAULT NULL,
  `id_reservas` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_huespedes_reservas1_idx` (`id_reservas`),
  CONSTRAINT `fk_huespedes_reservas1` FOREIGN KEY (`id_reservas`) REFERENCES `reservas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hotel.huespedes: ~7 rows (aproximadamente)
DELETE FROM `huespedes`;
/*!40000 ALTER TABLE `huespedes` DISABLE KEYS */;
INSERT INTO `huespedes` (`id`, `nombre`, `apellido`, `correo`, `pais`, `telefono`, `ciudad`, `comentario`, `id_reservas`) VALUES
	(1, 'John', 'Gutierrez', 'a@b.com', 'colombia', 55555, 'bogota', 'sin comentarios', 1),
	(2, 'Wilmar', 'Panche', 'b@r.com', 'colombia', 66666, 'bogota', 'sin comentarios', 2),
	(3, 'Wilmar', 'Panche', 'b@r.com', 'colombia', 66666, 'bogota', 'sin comentarios', 2),
	(5, 'string', 'string', 'string', 'string', 0, 'string', 'string', 1),
	(6, 'string', 'string', 'string', 'string', 0, 'string', 'string', 1),
	(7, 'maria', 'perez', 'aaaa', 'veneca', 0, 'caracas', 'no', 2),
	(8, 'string', 'string', 'string', 'string', 0, 'string', 'string', 8),
	(9, 'string', 'string', 'string', 'string', 0, 'string', 'string', 10),
	(10, 'maria', 'cruz', 'a@b.com', 'colombia', 555, 'bogota', 'sfg', 11),
	(11, 'maria', 'cruz', 'a@b.com', 'colombia', 555, 'bogota', 'sfg', 12),
	(12, 'camila', 'Gutierrez', 'a@b.com', 'colombia', 4444, 'bogota', 'serg', 13);
/*!40000 ALTER TABLE `huespedes` ENABLE KEYS */;

-- Volcando estructura para tabla hotel.reservas
DROP TABLE IF EXISTS `reservas`;
CREATE TABLE IF NOT EXISTS `reservas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_habitacion` int(11) NOT NULL,
  `fecha_entrada` date DEFAULT NULL,
  `fecha_salida` date DEFAULT NULL,
  `ocupacion_adulto` int(11) DEFAULT NULL,
  `ocupacion_nino` int(11) DEFAULT NULL,
  `precio_final` double(22,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reservas_habitaciones_idx` (`id_habitacion`),
  CONSTRAINT `fk_reservas_habitaciones` FOREIGN KEY (`id_habitacion`) REFERENCES `habitaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla hotel.reservas: ~8 rows (aproximadamente)
DELETE FROM `reservas`;
/*!40000 ALTER TABLE `reservas` DISABLE KEYS */;
INSERT INTO `reservas` (`id`, `id_habitacion`, `fecha_entrada`, `fecha_salida`, `ocupacion_adulto`, `ocupacion_nino`, `precio_final`) VALUES
	(1, 1, '2020-11-17', '2020-11-19', 1, 1, 50000.00),
	(2, 1, '2020-11-22', '2020-11-24', 1, 1, 600000.00),
	(3, 2, '2020-11-30', '2020-12-05', 2, 2, 80000.00),
	(4, 2, '2020-11-30', '2020-12-05', 2, 2, 80000.00),
	(5, 1, '2020-11-19', '2020-11-19', 0, 0, 0.00),
	(6, 1, '2020-11-23', '2020-11-27', 0, 0, 0.00),
	(7, 2, '2020-11-18', '2020-11-30', 1, 1, NULL),
	(8, 1, '2020-11-19', '2020-11-19', 0, 0, 0.00),
	(10, 1, '2020-11-22', '2020-11-22', 0, 0, 0.00),
	(11, 1, '2020-11-30', '2020-12-01', 1, 1, 249999.00),
	(12, 1, '2020-11-30', '2020-12-01', 1, 1, 249999.00),
	(13, 1, '2020-12-01', '2020-12-03', 1, 0, 249999.00);
/*!40000 ALTER TABLE `reservas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
